import os
import urllib.request

import cloudinary.uploader
import numpy as np
import tensorflow as tf
from flask import Flask, jsonify
from keras.preprocessing import image
from PIL import Image

import torch
import torchvision
from torchvision import datasets, models, transforms
from torchvision import transforms, utils

app = Flask(__name__)

cloudinary.config(
    cloud_name="digrubrgw",
    api_key="953684376456813",
    api_secret="ch1haynm_MVry9wbQrK84UgIdr0"
)

sample_data = [1.8257,3.2051,1.4232,7.4072,0.2794,1.7864,0.219,1.9131,0.255,0.0842,1.8746,3.5156,4.2709,0.9554]

label_baseline_probs_list = [0.103,0.025,0.119,0.177,0.051,0.056,0.012,0.047,0.042,0.021,0.022,0.015,0.03,0.002]

label_baseline_probs={
    'Atelectasis':0.103,
    'Cardiomegaly':0.025,
    'Effusion':0.119,
    'Infiltration':0.177,
    'Mass':0.051,
    'Nodule':0.056,
    'Pneumonia':0.012,
    'Pneumothorax':0.047,
    'Consolidation':0.042,
    'Edema':0.021,
    'Emphysema':0.022,
    'Fibrosis':0.015,
    'Pleural_Thickening':0.03,
    'Hernia':0.002
}

PATH_TO_MODEL = "reproduce-chexnet2/pretrained/checkpoint"

def load_image(image_url, format='png'):

    url = cloudinary.utils.cloudinary_url(image_url)
    urllib.request.urlretrieve(url[0], image_url)

    img = image.load_img(image_url, target_size=(224, 224))
    img_tensor = image.img_to_array(img, data_format='channels_first')  # (channels, height, width)
    # img_tensor = image.img_to_array(img)# (height, width, channels)
    print_image_data (img_tensor)

    img_tensor = np.expand_dims(img_tensor, axis=0)
    print_image_data (img_tensor)

    img_tensor /= 255.
    print_image_data (img_tensor)

    os.remove(image_url)

    return img_tensor

def load_image_pytorch(image_url, format='png'):

    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    data_transform = transforms.Compose([
        transforms.Scale(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean, std)
    ])

    url = cloudinary.utils.cloudinary_url(image_url)
    urllib.request.urlretrieve(url[0], image_url)

    image = Image.open(image_url)
    image = image.convert('RGB')
    image = data_transform(image)
    img_tensor = image.unsqueeze(0)

    os.remove(image_url)

    return img_tensor

def print_image_data(image_tensor):
    print  (image_tensor.shape)
    print  (image_tensor)
    print ('-------------------')

def load_pb(path_to_pb):
    with tf.gfile.GFile(path_to_pb, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def, name='')
        return graph


def get_result(img_tensor):
    tf_graph = load_pb('reproduce-chexnet2/pretrained/checkpoint2.tf')
    sess = tf.Session(graph=tf_graph)
    output_tensor = tf_graph.get_tensor_by_name('Sigmoid:0')
    input_tensor = tf_graph.get_tensor_by_name('input:0')
    output = sess.run(output_tensor, feed_dict={input_tensor: img_tensor})

    return output


'''
Devuelve resultados similares al modelo TensorFlow
Falta probar usando data_transform = transforms.Compose
'''
def predict_pytorch(input):
    checkpoint = torch.load(PATH_TO_MODEL, map_location=lambda storage, loc: storage)
    model = checkpoint['model']
    del checkpoint
    model.cpu()

    for i, (name, module) in enumerate(model._modules.items()):
        module = recursion_change_bn(model)

    original = input

    prediction = model(original).data.numpy()[0]

    return prediction

def recursion_change_bn(module):
    if isinstance(module, torch.nn.BatchNorm2d):
        module.track_running_stats = 1
    else:
        for i, (name, module1) in enumerate(module._modules.items()):
            module1 = recursion_change_bn(module1)
    return module

@app.route('/todo/api/v1.0/prediagnosis/<string:image_url>', methods=['GET'])
def get_prediagnosis(image_url):
    img_tensor = load_image_pytorch(image_url)
    # result = get_result(img_tensor)

    result = predict_pytorch(img_tensor)
    # img_tensor deberia ser un torch.tensor

    return jsonify({'result': result.tolist()})


if __name__ == '__main__':
    # http://res.cloudinary.com/digrubrgw/image/upload/v1566523367/rxg3y4xbfvog4qgdnomi.png
    # image_url = "rxg3y4xbfvog4qgdnomi.png"
    # image_url = "f5aipsq8svdge34jkpkr.png"
    # result = get_prediagnosis(image_url)
    # print(result)
    app.run(host="0.0.0.0", debug=True)

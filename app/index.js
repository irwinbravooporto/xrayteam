const express=require("express");
const path=require("path");
const config=require("./backend/config/config");
const rutas=require("./backend/routes/rutas");
const mongoose=require("mongoose");

const app=express();

/*************************CONFIGURACIONES ********************************* */

//MOTOR DE VISTAS
app.set("views",path.join(__dirname,"/backend/views"));
app.set("view engine","pug");




/************************************ MIDDLEWARES *****************************************/

//PARSEADO
app.use(express.urlencoded({extended:true}));
app.use(express.json());


//ENRUTAMIENTO
app.use(rutas);
//ARCHIVOS ESTÁTICOS
app.use(express.static(path.join(__dirname,"/frontend/public")));


/************************************SERVIDOR A LA ESCUCHA ******************************* */
//la aplicación tiene como condición que se haya establecido conexión con la bbdd
app.listen(config.puerto, () => {
    console.log('Estamos online y conectados a la bbdd');
});
/*
mongoose.connect(config.bbdd.URI,{useNewUrlParser:true,useCreateIndex:true}).then(db=>{
    app.listen(config.puerto, () => {
        console.log('Estamos online y conectados a la bbdd');
    });
})*/

